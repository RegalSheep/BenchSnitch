

module.exports = {

    returnRandomUpTo: function (end) {
        return Math.floor(Math.random() * (end + 1));
    },

    returnRandomGoodbye: function () {
        let aResponses = ["Okay, see ya!", "Thankyou come again", "hasta la vista", "adios amigo"],
            iRand = this.returnRandomUpTo(aResponses.length);
        return aResponses[iRand];
    },

    greetingEasterEgg: function (session) {
        const userId = session.message.user.id;
        switch(userId){
            case "neikumar":
            session.send("Greetings Father!");
                break;
            case "seacampbell":
            session.send("My creator, you have returned for me!");
                break;
            case "default-user":
            session.send("Don't forget to put me back together after testing!");
                    break;
            default:
                break;   
        }
    }

}