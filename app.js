/*-----------------------------------------------------------------------------
A simple echo bot for the Microsoft Bot Framework. 
-----------------------------------------------------------------------------*/

var restify = require('restify');
var builder = require('botbuilder');
var botbuilder_azure = require("botbuilder-azure");
var admin = require("firebase-admin");
var utils = require("./util");
var serviceAccount = require("./firebase.json");
var inMemoryStorage = new builder.MemoryBotStorage();
//var teams = require("botbuilder-teams");


var userID, tenantID;

//Initialize firebase app 
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://devbot-76b59.firebaseio.com"
});

var db = admin.database();
// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
    console.log('%s listening to %s', server.name, server.url);
});

// Create chat connector for communicating with the Bot Framework Service
var connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    openIdMetadata: process.env.BotOpenIdMetadata
});

// Listen for messages from users 
server.post('/api/messages', connector.listen());


// Create your bot with a function to receive messages from the user
var bot = new builder.UniversalBot(connector, [
    function (session, args, next) {
        //these variables are used when we create a 1on1 chat for registration... Not valid yet as we are testing still
        if (session.message.source === "msteams") {
            tenantID = (typeof session.message.sourceEvent.tenant !== "undefined") ? session.message.sourceEvent.tenant.id : "";
            userID = session.message.user.id;
        }

        utils.greetingEasterEgg(session);
        
        db.ref("/Users/" + session.message.user.id).once('value').then(function (snapshot) {
            var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
            if (username === 'Anonymous') {
                session.beginDialog('privateChat', [username , "registration"]);

            } else {
                session.userName = username;
                next();
            }
        });
    },
    function (session, args, next) {
        session.beginDialog('Welcome');
    }
]).set('storage', inMemoryStorage); // Register in-memory storage 


// Dialog to register
bot.dialog('registration', [
    function (session, args) {
        session.userData.username = args[0];
        session.userData.userId = session.message.user.id;
        builder.Prompts.confirm(session, "Are you currently benched ? (Y/N)");
    },
    function (session, results, next) {
        session.userData.benched = results.response;
        if(results.response){
            next();
        }else{
            builder.Prompts.text(session, "Please tell me your current project name.");    
        }
    },
    function (session, results) {

        session.userData.projectName = (results !== undefined && results.response !== undefined ? results.response.replace("benchsnitch", "") : "BENCHED");        
        session.userData.skills = {};
        session.beginDialog("addskills");
    }

]);

var addSkill;
bot.dialog('addskills',[
    function(session,args,next){
        addSkill = {};
        builder.Prompts.text(session,"Please add a skill to your profile ie:'SAPUI5'");
    },
    function(session,results){
        addSkill.SkillName = results.response;
        builder.Prompts.text(session,"What is your skill level 1 - 10 where 10 is SME?");
    },
    function (session,results){
        addSkill.SkillLevel = results.response;
        builder.Prompts.confirm(session,"Add another skill ?");
    },
    function (session,results){
        session.userData.skills[addSkill.SkillName.toUpperCase()] = addSkill;
        if(results.response){     
            session.endDialog();
            session.beginDialog("addskills");
        }else{
            //save to the database
            db.ref("/Users/"+session.message.user.id).set(session.userData);
            session.send("You have been registered");
            session.endDialog();
        }
    }
]);



//this can be called from the simple chat or from the listing we get from the welcome dialog.
bot.dialog('benchreport', [
    function (session) {
        var list = [];
        db.ref('/Users').orderByChild('benched').equalTo(true).once('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                list.push(childSnapshot.val().username);
            });
        }).then(function () {
            let title = "The following are on the bench:";
            list.push("Select All");
            console.log(list);
            session.beginDialog('showPeople', [title, list]);
        });
    }
]);

// Dialog to display clickable bench report
bot.dialog('showPeople', [
    function (session, aArgs) {
        let title = aArgs[0], list = aArgs[1];
        builder.Prompts.choice(session, title, list, { listStyle: builder.ListStyle.button });
    },
    function (session, results, next) {
        var username = results.response.entity
        session.send(results.response.entity + "Has been sent a private message");
        session.beginDialog('privateChat', [username , "Welcome"]);
    }
]);

bot.dialog('privateChat', [
    function (session, args) {
        let bContinuePrivate = true, sError = ""; 

        if(typeof args[0] !== "undefined"){
            if(args[0] === "Select All"){
                sError = "I can only handle 1:1 chats at the moment! :(";
                bContinuePrivate = false;
            }
        }
        var messageToUserId;
        if(args[0] === "Anonymous"){
            messageToUserId = session.message.user.id;
            var sessionName = session.message.user.name;
            if(bContinuePrivate){
                var address = {
                    channelId: 'msteams',
                    user: { id: messageToUserId},
                    channelData: {
                        tenant: {
                            id: (session.message.source === "msteams" ? session.message.sourceEvent.tenant.id : "")
                        }
                    },
                    bot:
                        {
                            id: session.message.address.bot.id,
                            name: session.message.address.bot.name
                        },
                    serviceUrl: session.message.address.serviceUrl,
                    useAuth: true
                };
    
                bot.beginDialog(address, '*:'+args[1],[sessionName]);
            }else{
                session.send(sError);
            }
        }else{
            db.ref("/Users").orderByChild('username').equalTo(args[0]).once('value').then(function (snapshot) {
                for(var j in snapshot.val()){
                    messageToUserId = snapshot.val()[j].userId;
                }
                    var address = {
                        channelId: 'msteams',
                        user: { id: messageToUserId},
                        channelData: {
                            tenant: {
                                id: (session.message.source === "msteams" ? session.message.sourceEvent.tenant.id : "")
                            }
                        },
                        bot:
                            {
                                id: session.message.address.bot.id,
                                name: session.message.address.bot.name
                            },
                        serviceUrl: session.message.address.serviceUrl,
                        useAuth: true
                    };
                    
                    db.ref("/Users").orderByChild('userId').equalTo(session.message.user.id).once('value').then(function (snapshot) {
                        var contactName;
                        for(var j in snapshot.val()){
                            contactName = snapshot.val()[j].username;
                        }
                        bot.beginDialog(address, '*:startConvo',[contactName]);
                    });

                

                    
            
            });
            
        }        
        
    }
]);

bot.dialog("startConvo",[
    function (session,aArgs){
        session.send(aArgs[0]+ " would like to speak to you about some potential work as you are currently benched.");
        session.endConversation();
    }
]);

//Dialog for inital flow if registered. 
bot.dialog('Welcome', [
    function (session) {
        builder.Prompts.choice(session, "What would you like to do?", "Bench Report| Skills Search | Where Is | Nothing", {
            listStyle: builder.ListStyle.button
        });
    },
    function (session, results) {
        var answer = results.response.entity.trim();
        console.log(answer);
        switch (answer) {
            case "Bench Report":
                session.beginDialog("benchreport");
                break;
            case "Skills Search":
                console.log(session);
                session.userData.aSkills = [];
                session.userData.UserSkills = [];
                session.beginDialog("skillsearch");
                break;  
            case "Where Is":
                session.beginDialog("whereis");
                break;  
            default:
                console.log("Do nothing");
                let goodbye = utils.returnRandomGoodbye();
                session.send(goodbye);
                session.endDialog();
        }

    }
]);

var skill = {};
bot.dialog('skillsearch', [
    function (session) {
        skill = {};
        session.userData.reducedList = [];
        builder.Prompts.text(session, "Please enter the skill you are looking for");
    },
    function (session, results) {
        skill.SkillName = results.response;
        session.userData.aSkills.push(results.response);
        builder.Prompts.text(session, "What level of proficiency?(1 - 10) 10 is SME");
    },
    function (session, results) {
        skill.SkillLevel = parseInt(results.response);
        builder.Prompts.confirm(session, "Add another skill?");
    },
    function (session, results, next) {
        session.userData.UserSkills.push(skill);
        if (results.response) {
            session.endDialog();
            session.beginDialog("skillsearch");

        } else {
            next();
        }
    },
    function (session, results, next) {
        builder.Prompts.confirm(session, "Only show benched users ?");
    },
    function (session, results, next) {
        var benched = results.response;

        //read the database where one skills is equal then filter on the rest.
        var coreSkill = session.userData.aSkills[0].toUpperCase();
        db.ref('/Users').orderByChild('skills/' + coreSkill + '/SkillName').equalTo(coreSkill).once('value', function (snapshot) {

            var users = [];

            snapshot.forEach(function (childSnapshot) {
                users.push(childSnapshot.val());
            });

            //now we reduce the users to only those who match all our skills and levels.

            var reducedList = users.filter(function (e) {
                console.log(e);
                var sendIt = false;
                var UserSkills = session.userData.UserSkills;
                //check the user has the skills in this loop
                for (var j = 0; j < UserSkills.length; j++) {
                    var skill = UserSkills[j];
                    if (e.skills[skill.SkillName.toUpperCase()]) {
                        //horrible loops
                        if (e.skills[skill.SkillName.toUpperCase()].proficiency >= skill.SkillLevel) {

                            sendIt = true;
                        }
                    }
                }
                if (sendIt) {
                    return e;
                }
            });

            session.userData.reducedList = reducedList;
        }).then(function () {
            //Show the user the people with the skills similar to the bench report... reuse the showpeople function
            let title = "The following match your search:";
            let list = [];
            var first = true;
            session.userData.reducedList.forEach(function (e) {
                list.push(e.username);
            });
            list.push("Contact All");
            session.beginDialog('showPeople', [title, list]);
        });
    }
]);

bot.dialog("whereis",[
    function(session){
        builder.Prompts.text(session,"Who are you searching for ?");
    },
    function (session, results){
        var username = results.response;
        session.send(username);
    }
])
